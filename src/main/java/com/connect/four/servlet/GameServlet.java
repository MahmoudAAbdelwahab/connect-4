package com.connect.four.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.connect.four.model.Game;
import com.connect.four.model.Player;

public class GameServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String viewsPath = "/WEB-INF/views/";

	@Override // url mappings for HTTP get requests
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String url = request.getRequestURL().toString();
		String loginName = ServletHelper.getLoginNameFromRequest(request);

		if (loginName != null) {
			if (url.endsWith("/")) {
				url = url.substring(0, url.length() - 1);
			}
			String[] params = url.split("/");

			// mapping for "/game/<opponent_name>" when logged in
			if (params[params.length - 2].equals("game")) {
				String opponentName = params[params.length - 1];
				doGetGame(loginName, opponentName, request, response);
			} // mapping for "/game_list/<opponent_name>" when logged in
			else if (params[params.length - 2].equals("game_table")) {
				String opponentName = params[params.length - 1];
				doGetGameTable(loginName, opponentName, request, response);
			} // mapping for "/legal/<opponent_name>/<col_no>" when logged in
			else if (params[params.length - 3].equals("legal")) {
				String opponentName = params[params.length - 2];
				int colNo = Integer.parseInt(params[params.length - 1]);
				isLegalMove(loginName, opponentName, colNo, request, response);
			} // mapping for "/play/<opponent_name>/<col_no>" when logged in
			else if (params[params.length - 3].equals("play")) {
				String opponentName = params[params.length - 2];
				int colNo = Integer.parseInt(params[params.length - 1]);
				doMove(loginName, opponentName, colNo, request, response);
			} else {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
			}

		} else {

			request.setAttribute("error_msg", "You're not logged in yet!");
			request.getRequestDispatcher("/index.jsp").forward(request, response);

		}

	}

	@Override // url mappings for HTTP post requests
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String url = request.getRequestURL().toString();
		String loginName = ServletHelper.getLoginNameFromRequest(request);

		if (loginName != null) {
			if (url.endsWith("/")) {
				url = url.substring(0, url.length() - 1);
			}
			String[] params = url.split("/");

			// mapping for "/play/<opponent_name>/<col_no>" when logged in
			if (params[params.length - 3].equals("play")) {
				String opponentName = params[params.length - 2];
				int colNo = Integer.parseInt(params[params.length - 1]);
				doMove(loginName, opponentName, colNo, request, response);
			} else {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
			}

		} else {

			request.setAttribute("error_msg", "You're not logged in yet!");
			request.getRequestDispatcher("/index.jsp").forward(request, response);

		}

	}

	@Override // url mappings for HTTP delete requests
	protected void doDelete(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String url = request.getRequestURL().toString();
		String loginName = ServletHelper.getLoginNameFromRequest(request);

		if (loginName != null) {
			if (url.endsWith("/")) {
				url = url.substring(0, url.length() - 1);
			}
			String[] params = url.split("/");

			// mapping for "/delete/<opponent_name>" when logged in
			if (params[params.length - 2].equals("delete")) {
				String opponentName = params[params.length - 1];
				doDeleteGame(loginName, opponentName, request, response);
			} else {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
			}

		} else {

			request.setAttribute("error_msg", "You're not logged in yet!");
			request.getRequestDispatcher("/index.jsp").forward(request, response);

		}

	}

	// fetches game then opens next page and feeds it with necessary arguments
	protected void doGetGame(String loginName, String opponentName, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try {
			Game game = Game.getGame(new Player(loginName), new Player(opponentName));

			request.setAttribute("game", game);
			request.setAttribute("turn", !game.turn ? game.playerOne.loginName : game.playerTwo.loginName);
			request.setAttribute("login_name", loginName);
			request.setAttribute("opponent_name", opponentName);

			request.getRequestDispatcher(viewsPath + "game.jsp").forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();

			response.getWriter().write("Error occurred, kindly reload the page");
		}

	}

	// fetches current state of the game and feeds it to client side
	protected void doGetGameTable(String loginName, String opponentName, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try {
			Game game = Game.getGame(new Player(loginName), new Player(opponentName));

			request.setAttribute("game", game);
			request.setAttribute("turn", !game.turn ? game.playerOne.loginName : game.playerTwo.loginName);

			request.getRequestDispatcher(viewsPath + "game_table.jsp").forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();

			if (e.getMessage().equals("Game fetching failed!")) {
				response.getWriter().write("logout");
			} else {
				response.getWriter().write("Error occurred, kindly reload the page");
			}
		}

	}

	// checks whether a move is legal and feeds response to client side
	protected void isLegalMove(String loginName, String opponentName, int colNo, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try {
			Player loginPlayer = new Player(loginName);
			Game game = Game.getGame(loginPlayer, new Player(opponentName));

			if (game.gameOver) {
				response.getWriter().write("over");
				return;
			}

			boolean turn = !game.playerOne.equals(loginPlayer);
			if (turn != game.turn) {
				response.getWriter().write("turn");
				return;
			}

			if (game.columnFull(colNo)) {
				response.getWriter().write("full");
				return;
			}

			response.getWriter().write("yes");
		} catch (SQLException e) {
			e.printStackTrace();

			response.getWriter().write("Error occurred, kindly reload the page");
		}

	}

	// updates the game state and feeds it to client side
	protected void doMove(String loginName, String opponentName, int colNo, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try {
			Game game = Game.getGame(new Player(loginName), new Player(opponentName));

			game.doMove(colNo);

			Game.updateGame(game);

			request.setAttribute("game", game);
			request.setAttribute("turn", !game.turn ? game.playerOne.loginName : game.playerTwo.loginName);

			request.getRequestDispatcher(viewsPath + "game_table.jsp").forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();

			response.getWriter().write("Error occurred, kindly reload the page");
		}

	}

	// deletes game
	protected void doDeleteGame(String loginName, String opponentName, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try {
			Game.deleteGame(new Player(loginName), new Player(opponentName));
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}