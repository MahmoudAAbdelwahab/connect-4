package com.connect.four.servlet;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public final class ServletHelper {

	// fetches login name from request, if a user is logged in
	// params: request
	public static String getLoginNameFromRequest(HttpServletRequest request) {

		String loginName = null;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("login_name")) {
					loginName = cookie.getValue();
				}
			}
		}

		return loginName;

	}

}
