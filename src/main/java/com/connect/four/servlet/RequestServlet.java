package com.connect.four.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.connect.four.model.Game;
import com.connect.four.model.Player;
import com.connect.four.model.Request;

@WebServlet("/request_servlet/*")
public class RequestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override // url mappings for HTTP get requests
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String url = request.getRequestURL().toString();
		String loginName = ServletHelper.getLoginNameFromRequest(request);

		if (loginName != null) {

			// mapping for "/check_request/"
			if (url.endsWith("/check_request/")) {
				doCheckRequest(loginName, request, response);
			} // mapping for "/follow_request/"
			else if (url.endsWith("/follow_request/")) {
				doFollowRequest(loginName, request, response);
			} else {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
			}

		} else {
			response.getWriter().write("error:Please login again!");
		}

	}

	@Override // url mappings for HTTP post requests
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String url = request.getRequestURL().toString();
		String loginName = ServletHelper.getLoginNameFromRequest(request);

		if (loginName != null) {
			String[] params = url.split("/");
			String opponentName = params[params.length - 1];

			// maping for "/send_request/<opponent_name>"
			if (params[params.length - 2].equals("send_request")) {
				doSendRequest(loginName, opponentName, request, response);
			} // mapping for "/accept_request/<opponent_name>"
			else if (params[params.length - 2].equals("accept_request")) {
				doAcceptRequest(loginName, opponentName, request, response);
			} else {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
			}

		} else {
			request.setAttribute("error_msg", "You're not logged in yet!");
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		}

	}

	@Override // url mappings for HTTP delete requests
	protected void doDelete(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String url = request.getRequestURL().toString();
		String loginName = ServletHelper.getLoginNameFromRequest(request);

		if (loginName != null) {
			String[] params = url.split("/");
			String opponentName = params[params.length - 1];

			// mapping for "/reject_request/<opponent_name>"
			if (params[params.length - 2].equals("reject_request")) {
				doRejectRequest(loginName, opponentName, request, response);
			} else {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
			}

		} else {
			request.setAttribute("error_msg", "You're not logged in yet!");
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		}

	}

	// fetches requests sent to player (one at a time) and
	// feeds them to client side
	protected void doCheckRequest(String loginName, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			Player opponent = Request.getRequesterForRequested(new Player(loginName));

			if (opponent != null) {
				response.getWriter().write("got:" + opponent.loginName);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	// follows already sent requests then
	// deletes responded-to requests and
	// feeds acceptance and rejection list to client side
	protected void doFollowRequest(String loginName, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			Player requester = new Player(loginName);
			String accept = "accept";
			ArrayList<Player> respondedToRequests = Request.followRequests(requester, "1");
			for (Player accepted : respondedToRequests) {
				Request.deleteRequest(requester, accepted);
				accept += ":" + accepted.loginName;
			}

			String reject = "reject";
			respondedToRequests = Request.followRequests(requester, "-1");
			for (Player rejected : respondedToRequests) {
				Request.deleteRequest(requester, rejected);
				reject += ":" + rejected.loginName;
			}

			String responseText = "";
			if (accept.equals("accept") && reject.equals("reject")) {
				responseText = "no";
			} else if (accept.equals("accept")) {
				responseText = "/" + reject;
			} else if (reject.equals("reject")) {
				responseText = accept + "/";
			} else {
				responseText = accept + "/" + reject;
			}

			response.getWriter().write(responseText);
		} catch (SQLException e) {
			e.printStackTrace();

			response.getWriter().write("error:Error occurred, kindly try again");
		}

	}

	// inserts requests if they are not available and
	// feeds response to client side
	protected void doSendRequest(String loginName, String opponentName, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try {
			Player requester = new Player(loginName), requested = new Player(opponentName);
			if (Request.findRequests(requester, requested)) {
				response.getWriter().write("success");
			} else {
				Request.insertRequest(requester, requested);

				response.getWriter().write("success");
			}
		} catch (SQLException e) {
			e.printStackTrace();

			response.getWriter().write("error:Error occurred, kindly try again");
		}

	}

	// creates a new game and updates request status to accepted
	protected void doAcceptRequest(String loginName, String opponentName, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try {
			Player requester = new Player(opponentName), requested = new Player(loginName);
			Game.insertGame(requester, requested);
			Request.updateRequestStatus(requester, requested, "1");

			response.getWriter().write("success");
		} catch (SQLException e) {
			e.printStackTrace();

			response.getWriter().write("error:Error occurred, kindly try again");
		}

	}

	// updates request status to rejected
	protected void doRejectRequest(String loginName, String opponentName, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try {
			Request.updateRequestStatus(new Player(opponentName), new Player(loginName), "-1");

			response.getWriter().write("success");
		} catch (SQLException e) {
			e.printStackTrace();

			response.getWriter().write("error:Error occurred, kindly try again");
		}

	}

}