package com.connect.four.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.connect.four.model.Game;
import com.connect.four.model.Player;
import com.connect.four.model.Request;

public class PlayerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String viewsPath = "/WEB-INF/views/";

	@Override // url mappings for HTTP get requests
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String url = request.getRequestURL().toString();
		String loginName = ServletHelper.getLoginNameFromRequest(request);

		if (loginName != null) {

			// mapping for "/online_list/" & "/online_list" when logged in
			if (url.endsWith("/online_list/") || url.endsWith("/online_list")) {
				doGetOnlineList(loginName, request, response);
			} // mapping for "/" & "/home" & "/home/" when logged in
			else if (url.endsWith("/home") || url.endsWith("/home/")
					|| (url.endsWith("/") && request.getRequestURI().toString().equals("/"))) {
				homeAndCreatePlayer(loginName, request, response);
			} else {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
			}

		} else {

			// mapping for "/" & "/home" & "/home/" when not logged in
			if (url.endsWith("/home") || url.endsWith("/home/")
					|| (url.endsWith("/") && request.getRequestURI().toString().equals("/"))) {
				request.getRequestDispatcher("/index.jsp").forward(request, response);
			} else {
				request.getRequestDispatcher(request.getRequestURI()).forward(request, response);
			}

		}

	}

	@Override // url mappings for HTTP delete requests
	protected void doDelete(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String url = request.getRequestURL().toString();
		String loginName = ServletHelper.getLoginNameFromRequest(request);

		if (loginName != null) {

			// mapping for "/logout/" & "/logout" when logged in
			if (url.endsWith("/logout/") || url.endsWith("/logout")) {
				doDeletePlayer(loginName, request, response);
			} else {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
			}

		} else {

			// mapping for "/logout/" when not logged in
			if (request.getRequestURL().toString().contains("/logout/")) {
				request.setAttribute("error_msg", "You're not logged in yet!");
				request.getRequestDispatcher("/index.jsp").forward(request, response);
			} else {
				request.getRequestDispatcher(request.getRequestURI()).forward(request, response);
			}

		}

	}

	// creates a new player if loginName does not exist then
	// opens home page and feeds it with necessary arguments
	// else redirects to landing page
	protected void homeAndCreatePlayer(String loginName, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String errorMessage = null, nextPage = "/index.jsp";
		ArrayList<Player> onlineList = null, requestList = null, gameList = null;

		if (!loginName.equals("")) {
			try {
				onlineList = Player.getPlayers();
				Player player = new Player(loginName);
				if (onlineList.contains(player)) {
					onlineList.remove(player);
				} else {
					Player.insertPlayer(loginName);
				}

				requestList = Request.getRequestedForRequester(player);
				gameList = Game.getGamePartners(player);
				nextPage = viewsPath + "home.jsp";
			} catch (SQLException e) {
				e.printStackTrace();

				errorMessage = "Error occured, kindly try again";
			}
		} else {
			errorMessage = "Oops, you forgot to enter a login name!";
		}

		errorMessage = errorMessage == null ? (String) request.getAttribute("error_msg") : errorMessage;

		request.setAttribute("online_list", onlineList);
		request.setAttribute("request_list", requestList);
		request.setAttribute("game_list", gameList);
		request.setAttribute("error_msg", errorMessage);

		request.getRequestDispatcher(nextPage).forward(request, response);

	}

	// fetches online player lists and feed them to client side
	protected void doGetOnlineList(String loginName, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ArrayList<Player> onlineList = null, requestList = null, gameList = null;
		try {
			onlineList = Player.getPlayers();
			Player player = new Player(loginName);
			onlineList.remove(player);

			requestList = Request.getRequestedForRequester(player);
			gameList = Game.getGamePartners(player);

			request.setAttribute("online_list", onlineList);
			request.setAttribute("request_list", requestList);
			request.setAttribute("game_list", gameList);

			request.getRequestDispatcher(viewsPath + "online_list.jsp").forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();

			response.getWriter().write("error:Error occured, kindly reload the page");
		}

	}

	// deletes player from db and responds to client side
	protected void doDeletePlayer(String loginName, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			Player.deletePlayer(new Player(loginName));

			response.getWriter().write("success");
		} catch (SQLException e) {
			e.printStackTrace();

			response.getWriter().write("error:Error occured, kindly try again");
		}

	}

}