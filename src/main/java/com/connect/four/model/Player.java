package com.connect.four.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.connect.four.db.DB;

public class Player {
	public static String tableName = "player";
	public String loginName;

	/** --------------------- instance methods --------------------- **/

	// class constructor
	public Player(String loginName) {
		this.loginName = loginName;
	}

	@Override // overrides java equals method
	public boolean equals(Object o) {
		return this.loginName.equals(((Player) o).loginName);
	}

	@Override // overrides java toString method
	public String toString() {
		return this.loginName;
	}

	/** --------------------- db methods --------------------- **/

	// creates table player in db
	public static boolean initializeTable() {
		System.out.println("Creating Table " + tableName + "..");

		try {
			String tableInitializationStatement = "CREATE TABLE " + tableName
					+ " (login_name VARCHAR(20) NOT NULL, PRIMARY KEY (login_name));";

			DB.execute(tableInitializationStatement);

			System.out.println("Table " + tableName + " created");

			return true;
		} catch (SQLException e) {
			System.out.println("Table " + tableName + " probably exists..\n" + e);
		}

		try {
			ResultSet rs = DB.getTables(tableName);
			rs.last();
			if (rs.getRow() > 0) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		System.out.println("Table " + tableName + " creation failed!");

		return false;
	}

	// fetches current online users from db
	public static ArrayList<Player> getPlayers() throws SQLException {
		String sql = "SELECT login_name FROM " + Player.tableName + ";";
		ResultSet rs = DB.executeQuery(sql);

		ArrayList<Player> players = new ArrayList<Player>();
		while (rs.next()) {
			players.add(new Player(rs.getString("login_name")));
		}

		return players;
	}

	// inserts a new player in db
	// params: loginName of the new player
	public static void insertPlayer(String loginName) throws SQLException {
		String sql = "INSERT INTO " + Player.tableName + " VALUES ('" + loginName + "');";

		if (DB.executeUpdate(sql) < 1) {
			throw new SQLException("Player adding failed");
		}
	}

	// deletes a player from db
	// params: player
	public static void deletePlayer(Player player) throws SQLException {

		Game.deletePlayerGames(player);
		Request.deletePlayerRequests(player);

		String sql = "DELETE FROM " + Player.tableName + " WHERE login_name='" + player.loginName + "';";
		if (DB.executeUpdate(sql) < 1) {
			throw new SQLException("Player deletion failed!");
		}
	}

}
