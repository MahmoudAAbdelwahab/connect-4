package com.connect.four.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.connect.four.db.DB;

public class Game {
	public static final String tableName = "game";
	public Player playerOne, playerTwo;
	public String status;
	public char winner;
	public boolean turn, gameOver;

	/** --------------------- instance methods --------------------- **/

	// class constructor
	public Game(Player playerOne, Player playerTwo, String status, boolean turn, boolean gameOver, char winner) {
		this.playerOne = playerOne;
		this.playerTwo = playerTwo;
		this.status = status;
		this.turn = turn;
		this.gameOver = gameOver;
		this.winner = winner;
	}

	// generic getter
	public Object getter(String key) {
		if (key.equals("player_one")) {
			return this.playerOne;
		}

		if (key.equals("player_two")) {
			return this.playerTwo;
		}

		if (key.equals("status")) {
			return this.status;
		}

		if (key.equals("turn")) {
			return this.turn;
		}

		if (key.equals("game_over")) {
			return this.gameOver;
		}

		if (key.equals("winner")) {
			return this.winner;
		}

		return null;
	}

	// checks whether a column is full
	// params: column number
	public boolean columnFull(int colNo) {
		return this.status.charAt(colNo) != ' ';
	}

	// updates game state locally, not in db
	// params: column number
	public void doMove(int colNo) {
		for (int cell = 35 + colNo; cell >= 0; cell -= 7) {
			if (this.status.charAt(cell) == ' ') {
				String prefix = this.status.substring(0, cell);
				String suffix = this.status.substring(cell + 1, 42);

				this.status = prefix + (turn ? '2' : '1') + suffix;
				break;
			}
		}

		this.changeTurn();
		this.checkWinner();
		this.isGameOver();
	}

	// toggles turn
	private void changeTurn() {
		this.turn = !this.turn;
	}

	// calls helper methods to check for "connected-4"
	private void checkWinner() {
		char winnerSoFar = this.checkRows();

		if (winnerSoFar != '0') {
			this.winner = winnerSoFar;
			return;
		}

		winnerSoFar = checkColumns();

		if (winnerSoFar != '0') {
			this.winner = winnerSoFar;
			return;
		}

		this.winner = checkDiagonals();
	}

	// checks for a "connected-4" in rows
	private char checkRows() {
		char winnerSoFar = '0';
		for (int row = 5; row >= 0 && winnerSoFar == '0'; row--) {
			int endCell = row * 7 + 4;
			for (int cell = endCell - 4; cell < endCell; cell++) {
				char print = this.status.charAt(cell);
				if (print != ' ' && print == this.status.charAt(cell + 1) && print == this.status.charAt(cell + 2)
						&& print == this.status.charAt(cell + 3)) {
					winnerSoFar = print;

					break;
				}
			}
		}

		return winnerSoFar;
	}

	// checks for a "connected-4" in columns
	private char checkColumns() {
		char winnerSoFar = '0';
		for (int col = 0; col < 7 && winnerSoFar == '0'; col++) {
			int endCell = col + 21;
			for (int cell = col + 35; cell >= endCell; cell -= 7) {
				char print = this.status.charAt(cell);
				if (print != ' ' && print == this.status.charAt(cell - 7) && print == this.status.charAt(cell - 14)
						&& print == this.status.charAt(cell - 21)) {
					winnerSoFar = print;

					break;
				}
			}
		}

		return winnerSoFar;
	}

	// check for a "connected-4" in diagonals
	private char checkDiagonals() {
		char winnerSoFar = '0';
		for (int cell = 41; cell > 20; cell--) {
			char print = this.status.charAt(cell);
			if (cell % 7 < 4) {
				if (print != ' ' && print == this.status.charAt(cell - 6) && print == this.status.charAt(cell - 12)
						&& print == this.status.charAt(cell - 18)) {
					winnerSoFar = print;

					break;
				}
			}

			if (cell % 7 > 3) {
				if (print != ' ' && print == this.status.charAt(cell - 8) && print == this.status.charAt(cell - 16)
						&& print == this.status.charAt(cell - 24)) {
					winnerSoFar = print;

					break;
				}
			}
		}

		return winnerSoFar;
	}

	// updates game over state locally, not in db
	private void isGameOver() {
		this.gameOver = this.gameOver || !this.status.contains(" ") || this.winner != '0';
	}

	/** --------------------- db methods --------------------- **/

	// create table game in db
	public static boolean initializeTable() {
		System.out.println("Creating Table " + tableName + "..");

		try {
			String tableInitializationStatement = "CREATE TABLE " + tableName + " (player_one VARCHAR(20) NOT NULL,"
					+ " player_two VARCHAR(20) NOT NULL,"
					+ " status VARCHAR(42) DEFAULT '                                          ',"
					+ " turn VARCHAR(1) DEFAULT '1'," + " game_over VARCHAR(1) DEFAULT '0',"
					+ " winner VARCHAR(1) DEFAULT '0'," + " FOREIGN KEY (player_one) REFERENCES player(login_name),"
					+ " FOREIGN KEY (player_two) REFERENCES player(login_name));";

			DB.execute(tableInitializationStatement);

			System.out.println("Table " + tableName + " created");

			return true;
		} catch (SQLException e) {
			System.out.println("Table " + tableName + " probably exists..\n" + e);
		}

		try {
			ResultSet rs = DB.getTables(tableName);
			rs.last();
			if (rs.getRow() > 0) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		System.out.println("Table " + tableName + " creation failed!");

		return false;
	}

	// fetches game from db
	// params: playerOne and playerTwo
	public static Game getGame(Player playerOne, Player playerTwo) throws SQLException {
		String sql = "SELECT * FROM " + Game.tableName + " WHERE (player_one='" + playerOne.loginName
				+ "' AND player_two='" + playerTwo.loginName + "') OR (player_one='" + playerTwo.loginName
				+ "' AND player_two='" + playerOne.loginName + "');";

		ResultSet rs = DB.executeQuery(sql);
		rs.last();
		if (rs.getRow() < 1) {
			throw new SQLException("Game fetching failed!");
		}

		return new Game(new Player(rs.getString("player_one")), new Player(rs.getString("player_two")),
				rs.getString("status"), rs.getString("turn").equals("2"), rs.getString("game_over").equals("1"),
				rs.getString("winner").charAt(0));
	}

	// fetches from db list of game partners who share a game with a player
	// params: player
	public static ArrayList<Player> getGamePartners(Player player) throws SQLException {
		String sql = "SELECT player_one, player_two FROM " + Game.tableName + " WHERE player_one='" + player.loginName
				+ "' OR player_two='" + player.loginName + "';";
		ResultSet rs = DB.executeQuery(sql);

		ArrayList<Player> gameList = new ArrayList<Player>();
		while (rs.next()) {
			String player_name = rs.getString("player_one");
			if (player_name.equals(player.loginName)) {
				gameList.add(new Player(rs.getString("player_two")));
			} else {
				gameList.add(new Player(player_name));
			}
		}

		return gameList;
	}

	// inserts new game in db
	// params: playerOne and playerTwo
	public static void insertGame(Player playerOne, Player playerTwo) throws SQLException {
		String sql = "INSERT INTO " + Game.tableName + "(player_one, player_two) VALUES ('" + playerOne.loginName
				+ "', '" + playerTwo.loginName + "');";

		if (DB.executeUpdate(sql) < 1) {
			throw new SQLException("Game adding failed!");
		}
	}

	// updates a game in db
	// params: playerOne and playerTwo
	public static void updateGame(Game game) throws SQLException {
		String sql = "UPDATE " + Game.tableName + " SET status='" + game.status + "', turn='" + (game.turn ? '2' : '1')
				+ "', game_over='" + (game.gameOver ? '1' : '0') + "', winner='" + game.winner + "' WHERE (player_one='"
				+ game.playerOne + "' AND player_two='" + game.playerTwo + "');";

		if (DB.executeUpdate(sql) < 1) {
			throw new SQLException("Game update failed!");
		}
	}

	// deletes game between players from db
	// params: playerOne and playerTwo
	public static void deleteGame(Player playerOne, Player playerTwo) throws SQLException {
		String sql = "DELETE FROM " + Game.tableName + " WHERE (player_one='" + playerOne.loginName
				+ "' AND player_two='" + playerTwo.loginName + "') OR (player_one='" + playerTwo.loginName
				+ "' AND player_two='" + playerOne.loginName + "');";

		DB.executeUpdate(sql);
	}

	// deletes all games related to player from db
	// params: player
	public static void deletePlayerGames(Player player) throws SQLException {
		String sql = "DELETE FROM " + Game.tableName + " WHERE player_one='" + player.loginName + "' OR player_two='"
				+ player.loginName + "';";
		DB.executeUpdate(sql);
	}

}
