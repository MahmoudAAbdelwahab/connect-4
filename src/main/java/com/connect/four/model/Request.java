package com.connect.four.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.connect.four.db.DB;

public class Request {
	public static final String tableName = "request";
	public Player requester, requested;

	/** --------------------- instance methods --------------------- **/

	// class constructor
	public Request(Player requester, Player requested) {
		this.requester = requester;
		this.requested = requested;
	}

	/** --------------------- db methods --------------------- **/

	// creates table request in db
	public static boolean initializeTable() {
		System.out.println("Creating Table " + tableName + "..");

		try {
			String tableInitializationStatement = "CREATE TABLE " + tableName
					+ " (requester VARCHAR(20) NOT NULL, requested VARCHAR(20) NOT NULL, accepted VARCHAR(2) DEFAULT '0', FOREIGN KEY (requester) REFERENCES player(login_name), FOREIGN KEY (requested) REFERENCES player(login_name), PRIMARY KEY (requester, requested));";

			DB.execute(tableInitializationStatement);

			System.out.println("Table " + tableName + " created");

			return true;
		} catch (SQLException e) {
			System.out.println("Table " + tableName + " probably exists..\n" + e);
		}

		try {
			ResultSet rs = DB.getTables(tableName);
			rs.last();
			if (rs.getRow() > 0) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		System.out.println("Table " + tableName + " creation failed!");

		return false;
	}

	// fetches list of players for which game requests have been sent
	// params: player who requested to start a game
	public static ArrayList<Player> getRequestedForRequester(Player player) throws SQLException {
		String sql = "SELECT requested FROM " + Request.tableName + " WHERE requester='" + player.loginName + "';";
		ResultSet rs = DB.executeQuery(sql);

		ArrayList<Player> requestList = new ArrayList<Player>();
		while (rs.next()) {
			requestList.add(new Player(rs.getString("requested")));
		}

		return requestList;
	}

	// fetches a player from those who sent game requests to player
	// params: player who received a request to start a game
	public static Player getRequesterForRequested(Player player) throws SQLException {
		String sql = "SELECT * FROM " + Request.tableName + " WHERE requested='" + player.loginName + "';";

		ResultSet rs = DB.executeQuery(sql);
		rs.next();
		if (rs.getRow() > 0) {
			return new Player(rs.getString("requester"));
		}

		return null;
	}

	// checks whether any requests are sent from one player to another
	// params: playerOne and playerTwo
	public static boolean findRequests(Player playerOne, Player playerTwo) throws SQLException {
		String sql = "SELECT * FROM " + Request.tableName + " WHERE (requester='" + playerOne.loginName
				+ "' AND requested='" + playerTwo.loginName + "') OR (requester='" + playerTwo.loginName
				+ "' AND requested='" + playerOne.loginName + "');";

		ResultSet rs = DB.executeQuery(sql);
		rs.last();
		return rs.getRow() > 0;
	}

	// fetches list of players who responded to a sent request from player
	// params: player, response status 1 for accepted, -1 for rejected
	public static ArrayList<Player> followRequests(Player player, String responseStatus) throws SQLException {
		String sql = "SELECT requested FROM " + Request.tableName + " WHERE requester='" + player.loginName
				+ "' AND accepted='" + responseStatus + "';";

		ResultSet rs = DB.executeQuery(sql);
		ArrayList<Player> respondedToRequests = new ArrayList<Player>();
		while (rs.next()) {
			respondedToRequests.add(new Player(rs.getString("requested")));

		}

		return respondedToRequests;
	}

	// inserts a new request in db
	// params: requester and requested players
	public static void insertRequest(Player requester, Player requested) throws SQLException {
		String sql = "INSERT INTO " + Request.tableName + "(requester, requested) VALUES ('" + requester.loginName
				+ "', '" + requested.loginName + "');";

		if (DB.executeUpdate(sql) < 1) {
			throw new SQLException("Request adding failed!");
		}
	}

	// updates a request in db
	// params: requester and requested players
	// params: requestStatus "1" for accept and "-1" for reject
	public static void updateRequestStatus(Player requester, Player requested, String requestStatus)
			throws SQLException {
		String sql = "UPDATE " + Request.tableName + " SET accepted='" + requestStatus + "' WHERE (requester='"
				+ requester.loginName + "' AND requested='" + requested.loginName + "');";

		if (DB.executeUpdate(sql) < 1) {
			throw new SQLException("Request update failed!");
		}
	}

	// deletes a request from db
	// params: requester and requested players
	public static void deleteRequest(Player playerOne, Player playerTwo) throws SQLException {
		String sql = "DELETE FROM " + Request.tableName + " WHERE (requester='" + playerOne.loginName
				+ "' AND requested='" + playerTwo.loginName + "') OR (requester='" + playerTwo.loginName
				+ "' AND requested='" + playerOne.loginName + "');";

		if (DB.executeUpdate(sql) < 1) {
			throw new SQLException("Request deletion failed!");
		}
	}

	// deletes all requests sent from/to player from db
	// params: player
	public static void deletePlayerRequests(Player player) throws SQLException {
		String sql = "DELETE FROM " + Request.tableName + " WHERE requester='" + player.loginName + "' OR requested='"
				+ player.loginName + "';";
		DB.executeUpdate(sql);
	}

}
