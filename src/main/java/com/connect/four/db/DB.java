package com.connect.four.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DB {

	/** --------------------- db properties --------------------- **/
	public static final String serverName = "localhost:3306";
	public static final String dbName = "connect_four";
	public static final String url = "jdbc:mysql://" + serverName + "/" + dbName;
	public static final String username = "root";
	public static final String password = "";

	public static Connection connection;

	/** --------------------- db methods --------------------- **/

	// fetches query results from db
	// params: query in MySQL lang
	public static ResultSet executeQuery(String query) throws SQLException {
		Statement statement = connection.createStatement();

		return statement.executeQuery(query);
	}

	// fetches number of affected rows of update query in db
	// params: query in MySQL lang
	public static int executeUpdate(String query) throws SQLException {
		Statement statement = connection.createStatement();

		return statement.executeUpdate(query);
	}

	// fetches whether a query is executed successfully in db
	// params: query in MySQL lang
	public static boolean execute(String query) throws SQLException {
		Statement statement = connection.createStatement();

		return statement.execute(query);
	}

	// fetches tables that match a certain table name in db
	// params: table name
	public static ResultSet getTables(String tableName) throws SQLException {
		return connection.getMetaData().getTables(null, null, tableName, null);
	}

}
