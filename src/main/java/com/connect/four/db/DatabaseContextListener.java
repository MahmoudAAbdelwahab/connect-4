package com.connect.four.db;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.connect.four.model.Game;
import com.connect.four.model.Player;
import com.connect.four.model.Request;

public class DatabaseContextListener implements ServletContextListener {

	// creates db and tables if they are not already created
	// method is called on project launch
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		System.out.println("Starting up!\n" + "Initializing MySql Connection..");

		try {
			DB.connection = DriverManager.getConnection(DB.url.replace(DB.dbName, ""), DB.username, DB.password);
			System.out.println("MySql Connection initialized");
		} catch (SQLException e) {
			throw new IllegalStateException("MySql Connection failed!", e);
		}

		System.out.println("Creating Database " + DB.dbName + "..");

		try {
			String dbInitializationStatement = "CREATE DATABASE " + DB.dbName + ";";
			Statement stmt = DB.connection.createStatement();
			stmt.execute(dbInitializationStatement);

			System.out.println("Database " + DB.dbName + " created");
		} catch (SQLException e) {
			System.out.println("Database " + DB.dbName + " probably exists..\n" + e);
		}

		System.out.println("Initializing Database Connection..");

		try {
			DB.connection = DriverManager.getConnection(DB.url, DB.username, DB.password);
			System.out.println("Database Connection initialized");
		} catch (SQLException e) {
			throw new IllegalStateException("Database Connection failed!", e);
		}

		Player.initializeTable();
		Game.initializeTable();
		Request.initializeTable();
	}

	// terminates db connection
	// method is called on project shut down
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		if (DB.connection != null) {
			try {
				DB.connection.close();
				System.out.println("Database Connection terminated");
			} catch (Exception e) {
				System.out.println("Database Disconnection failed!\n" + e);
			}
		}

		System.out.println("Shutting down!");
	}
}