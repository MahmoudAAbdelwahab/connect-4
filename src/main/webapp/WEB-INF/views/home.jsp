<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Home of Connect-4</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="javascript/home.js"></script>
</head>
<body>
	<button style="float: right; background: #FF0000;" id="logout_btn">Logout</button>
	<h1 id="welcome_msg">Welcome</h1>
	<c:if test="${!empty requestScope['error_msg']}">
		<p style="display: inline; color: #FF0000;">${requestScope['error_msg']}</p>
	</c:if>
	<br />
	<h2>List of online users</h2>

	<div id="online_list">
		<jsp:include page="/WEB-INF/views/online_list.jsp">
			<jsp:param name="requestScope" value="${requestScope}" />
		</jsp:include>
	</div>
</body>
</html>