<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:forEach var="online_user" items="${requestScope['online_list']}">
	<div style="display: inline;" class="div_users" id="div_${online_user}">
		<c:out value="${online_user}" />
		<c:choose>
			<c:when test="${requestScope['request_list'].contains(online_user)}">
				<button style="display: none; background: #AAAAAA" class="play_now"
					id="play_${online_user}">Play now!</button>
				<p style="display: inline; color: #00BB55;"
					id="loading_${online_user}">request sent..</p>
				<p style="display: none; color: #0055BB;" id="ingame_${online_user}">
					You're already in a game..
					<button style="background: #AAAAAA" class="open_game"
						id="game_${online_user}">open game</button>
				</p>
			</c:when>
			<c:when test="${requestScope['game_list'].contains(online_user)}">
				<button style="display: none; background: #AAAAAA" class="play_now"
					id="play_${online_user}">Play now!</button>
				<p style="display: none; color: #00BB55;"
					id="loading_${online_user}">request sent..</p>
				<p style="display: inline; color: #0055BB;"
					id="ingame_${online_user}">
					You're already in a game..
					<button style="background: #AAAAAA" class="open_game"
						id="game_${online_user}">open game</button>
				</p>
			</c:when>
			<c:otherwise>
				<button style="display: inline; background: #AAAAAA"
					class="play_now" id="play_${online_user}">Play now!</button>
				<p style="display: none; color: #00BB55;"
					id="loading_${online_user}">request sent..</p>
				<p style="display: none; color: #0055BB;" id="ingame_${online_user}">
					You're already in a game..
					<button style="background: #AAAAAA" class="open_game"
						id="game_${online_user}">open game</button>
				</p>
			</c:otherwise>
		</c:choose>
	</div>
	<br />
</c:forEach>