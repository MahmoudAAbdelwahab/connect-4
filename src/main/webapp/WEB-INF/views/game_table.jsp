<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="status" value="${requestScope['game'].getter('status')}" />
<br />
<table style="margin: auto; background: #0000FF;" id="game_table">
	<c:forEach var="row" begin="0" end="5">
		<tr id="row_${row}">
			<c:set var="start" value="${row * 7}" />
			<c:forEach var="cell" begin="${start}" end="${start + 6}" step="1">
				<c:set var="val" value="${fn:substring(status, cell, cell + 1)}" />
				<c:choose>
					<c:when test="${val == '1'}">
						<td style="background: #FF0000;" class="col_${cell % 7} circle"
							id="cell_${cell}"></td>
					</c:when>
					<c:when test="${val == '2'}">
						<td style="background: #FFFF00;" class="col_${cell % 7} circle"
							id="cell_${cell}"></td>
					</c:when>
					<c:otherwise>
						<td style="background: #FFFFFF;" class="col_${cell % 7} circle"
							id="cell_${cell}"></td>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</tr>
	</c:forEach>
</table>
<br />
<c:set var="winner"
	value="${fn:substring(requestScope['game'].getter('winner'), 0, 1)}" />
<c:choose>
	<c:when test="${winner == '1'}">
		<p style="text-align: center; color:" id="game_over">
			Game is over,
			<c:out value="${requestScope['game'].getter('player_one')}" />
			won!
		</p>
	</c:when>
	<c:when test="${winner == '2'}">
		<p style="text-align: center; color:" id="game_over">
			Game is over,
			<c:out value="${requestScope['game'].getter('player_two')}" />
			won!
		</p>
	</c:when>
	<c:when test="${requestScope['game'].getter('game_over')}">
		<p style="text-align: center; color:" id="game_over">Game is over!
		</p>
	</c:when>
	<c:otherwise>
		<p style="text-align: center;" id="turn">${requestScope['turn']}'s
			turn</p>
	</c:otherwise>
</c:choose>
<style>
table {
	font-family: arial, sans-serif;
	border-collapse: collapse;
	width: 70%;
	height: 50%;
}

td, th {
	border: 1px solid #dddddd;
	text-align: center;
	padding: 8px;
	width: 14%;
	height: 14%;
}

.circle {
	width: 50px;
	height: 50px;
	-webkit-border-radius: 25px;
	-moz-border-radius: 25px;
	border-radius: 25px;
}
</style>