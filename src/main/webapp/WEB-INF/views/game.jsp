<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
<title>Game with <c:out value="${requestScope['opponent_name']}" /></title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/javascript/game.js"></script>
</head>
<body>
	<H2 style="display: inline;">
		<c:out value="${requestScope['login_name']}" />
		vs.
		<c:out value="${requestScope['opponent_name']}" />
	</H2>
	<div id="div_game">
		<jsp:include page="/WEB-INF/views/game_table.jsp">
			<jsp:param name="requestScope" value="${requestScope}" />
		</jsp:include>
	</div>
</body>
</html>