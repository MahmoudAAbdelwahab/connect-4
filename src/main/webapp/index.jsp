<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Web Connect-4</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="/javascript/index.js"></script>
</head>
<body>
	<h1>Welcome to Connect-4!</h1>
	<input type="text" placeholder="enter your login name.."
		id="login_text" />
	<button id="login_btn" style="background: #FFFFFF;">Login</button>

	<c:if test="${!empty requestScope['error_msg']}">
		<p style="display: inline;" id="error_msg">
			<c:out value="${requestScope['error_msg']}" />
		</p>
		<c:set var="error_msg" value="${null}" scope="request" />
	</c:if>
</body>
</html>