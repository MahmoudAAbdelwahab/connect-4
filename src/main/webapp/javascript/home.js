$(function() {
	// function to be set to repeat with setInterval
	function intervalFn() {
		updateOnlineList();
		checkGameRequest();
		followGameRequest();
	}

	// sets interval and opened windows list
	var interval = setInterval(intervalFn, 5000);
	var windows = [];

	// fetches login user from cookies
	var login_name = document.cookie.split(';');
	for (var i = 0; i < login_name.length; i++) {
		if (login_name[i].search('login_name') > -1) {
			login_name = login_name[i].split('=')[1];
			break
		}
	}

	// sets login name in the welcome message
	$('#welcome_msg').html($('#welcome_msg').html() + ' ' + login_name);

	// binds logout button to click listener
	$('#logout_btn').click(function(e) {
		if (confirm('Are you sure you want to Logout?')) {
			logout();
		}
	});

	// binds statically and dynamically added children of selected div
	// to click listener
	// sends game request to opponent
	$('#online_list').on('click', '.play_now', function(e) {
		var opponent_name = $(this).attr('id').replace('play_', '');
		sendGameRequest(opponent_name);
	});

	// binds statically and dynamically added children of selected div
	// to click listener
	// opens game tab, if already opened focuses on it
	$('#online_list').on(
			'click',
			'.open_game',
			function(e) {
				var opponent_name = $(this).attr('id').replace('game_', '');
				var href = window.location.href.replace(
						window.location.pathname, '/game/' + opponent_name);
				var open;
				windows.forEach(function(window) {
					if (window.opener && window.location.href == href) {
						window.focus();
						open = true;

						return;
					}
				});

				if (!open) {
					windows.push(window.open(href));
				}
			});

	// fetches updated list of online players
	function updateOnlineList() {
		console.log('update');
		$('#online_list').load('/online_list/');
	}

	// fetches requests sent to login user
	function checkGameRequest() {
		console.log('check');
		$.get('/check_request/',
				function(responseText) {
					if (responseText.startsWith('got')) {
						clearInterval(interval);
						var opponent_name = responseText.split(':')[1];
						if (confirm(opponent_name
								+ ' wants to start a game with you')) {
							acceptGameRequest(opponent_name);
							interval = setInterval(intervalFn, 5000);
						} else {
							rejectGameRequest(opponent_name);
							interval = setInterval(intervalFn, 5000);
						}
					}
				});
	}

	// fetches responses on requests sent by login user
	function followGameRequest() {
		console.log('follow');
		$.get('/follow_request/', function(responseText) {
			if (!responseText.startsWith('error')
					&& !responseText.startsWith('no')) {
				var accept = responseText.split('/')[0];
				if (accept.startsWith('accept')) {
					console.log("follow:accept");
					var opponents = accept.replace('accept:', '').split(':');
					opponents.forEach(function(opponent_name) {
						$('#play_' + opponent_name).css('display', 'none');
						$('#loading_' + opponent_name).css('display', 'none');
						$('#ingame_' + opponent_name).css('display', 'inline');

						windows.push(window.open('/game/' + opponent_name));
					});
				}

				var reject = responseText.split('/')[1];
				if (reject.startsWith('reject')) {
					console.log('follow:reject');
					var opponents = reject.replace('reject:', '').split(':');
					opponents.forEach(function(opponent_name) {
						$('#play_' + opponent_name).css('display', 'inline');
						$('#loading_' + opponent_name).css('display', 'none');
						$('#ingame_' + opponent_name).css('display', 'none');
					});
				}

			}
		});
	}

	// sends request to opponent
	// params: opponent's login name
	function sendGameRequest(opponent_name) {
		console.log('send');
		$.post('/send_request/' + opponent_name, function(responseText) {
			if (responseText.startsWith('success')) {
				$('#play_' + opponent_name).css('display', 'none');
				$('#loading_' + opponent_name).css('display', 'inline');
				$('#ingame_' + opponent_name).css('display', 'none');
			} else if (responseText.startsWith('error')) {
				alert(responseText.split(':')[1]);
			} else {
				alert('Error occurred, kindly try again');
			}
		}).fail(function() {
			alert('Error occurred, kindly try again');
		});
	}

	// sends accept response on request sent by opponent
	// params: opponent's login name
	function acceptGameRequest(opponent_name) {
		console.log('accept');
		$.post('/accept_request/' + opponent_name, function(responseText) {
			if (responseText.startsWith('success')) {
				$('#play_' + opponent_name).css('display', 'none');
				$('#loading_' + opponent_name).css('display', 'none');
				$('#ingame_' + opponent_name).css('display', 'inline');

				windows.push(window.open('/game/' + opponent_name));
			} else if (responseText.startsWith('error')) {
				alert(responseText.split(':')[1]);
			} else {
				alert('Error occurred, kindly try again');
			}
		}).fail(function() {
			alert('Error occurred, kindly try again');
		});
	}

	// sends reject response on request sent by opponent
	// params: opponent's login name
	function rejectGameRequest(opponent_name) {
		console.log('reject');
		$.ajax({
			url : '/reject_request/' + opponent_name,
			method : 'DELETE',
			success : function(responseText) {
				if (responseText.startsWith('error')) {
					console.log('error:' + responseText);
					rejectGameRequest(opponent_name);
				}
			},
			error : function(responseText) {
				rejectGameRequest(opponent_name);
			}
		});
	}

	// sends logout notice to server side
	function logout() {
		console.log('logout');
		$
				.ajax({
					url : '/logout/',
					method : 'DELETE',
					success : function(responseText) {
						if (responseText.startsWith('success')) {
							console.log('logout:success');
							document.cookie = 'login_name=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
							closeWindows();
							window.location = '/';
						} else if (responseText.startsWith('error')) {
							console.log('error:' + responseText);
							logout();
						}
					},
					error : function(responseText) {
						console.log('error..' + responseText);
						logout();
					}
				});
	}

	// closes all tabs/windows opened from this tab
	function closeWindows() {
		windows.forEach(function(window) {
			if (window) {
				window.close();
			}
		});
	}
});