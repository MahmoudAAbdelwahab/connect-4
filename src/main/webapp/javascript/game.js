$(function() {
	// sets interval
	var interval = setInterval(function() {
		keepLive();
	}, 2000);

	// fetches opponent's name from URL
	var opponent_name = window.location.pathname.split('/');
	opponent_name = opponent_name[opponent_name.length - 1];

	// fetches login name from cookies
	var login_name = document.cookie.split(';');
	for (var i = 0; i < login_name.length; i++) {
		if (login_name[i].search('login_name') > -1) {
			login_name = login_name[i].split('=')[1];
			break;
		}
	}

	// sets login name in page's header
	$('#login_name').attr('value', login_name);

	// binds statically and dynamically added children of selected div
	// to click listener
	// checks if move is legal, if so does it
	$('#div_game').on(
			'click',
			'.col_0, .col_1, .col_2, .col_3, .col_4, .col_5, .col_6',
			function(e) {
				var url_suffix = opponent_name
						+ '/'
						+ $(this).attr('class').split(' ')[0].replace('col_',
								'');
				$.get('/legal/' + url_suffix, function(responseText) {
					if (responseText.startsWith('yes')) {
						doMove(url_suffix);
					} else if (responseText.startsWith('full')) {
						alert('The column is full!');
					} else if (responseText.startsWith('turn')) {
						alert('It\'s not your turn!');
					} else if (responseText.startsWith('over')) {
						alert('Game is over!');
					} else {
						alert('Error occurred, kindly try again');
					}
				}).fail(function() {
					alert('Error occurred, kindly try again');
				});
			});

	// binds window to unload listener
	// deletes game if over
	$(window).bind('unload', function() {
		if ($('#game_over').length) {
			$.ajax({
				url : '/delete/' + opponent_name,
				method : 'DELETE'
			});
		}
	});

	// sends doMove to server side
	// params: url suffix of doMove request url
	function doMove(url_suffix) {
		$.post('/play/' + url_suffix, function(responseText) {
			if (responseText.startsWith('Error')) {
				alert(responseText);
			} else {
				$('#div_game').html(responseText);
			}
		}).fail(function() {
			alert('Error occurred, kindly try again');
		});
	}

	// fetches updated game state
	function keepLive() {
		if ($('#game_over').length) {
			clearInterval(interval);
		}

		$.get('/game_table/' + opponent_name, function(responseText) {
			if (responseText.startsWith('Error')) {
				alert(responseText);
			} else if (responseText.startsWith('logout')) {
				alert(opponent_name + ' has logged out!');
				window.close();
			} else {
				$('#div_game').html(responseText);
			}
		}).fail(function() {
			alert('Error occurred, kindly try again');
		});
	}
});