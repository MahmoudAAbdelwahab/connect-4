$(function() {
	// clears login user from cookies if error message is received
	if ($('#error_msg').length) {
		document.cookie = 'login_name=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	}

	// prevents default behavior in selected field on return key press
	$('#login_text').keypress(function(e) {
		if (e.keyCode == 13) {
			e.preventDefault();
		}
	});

	// provides alternative behavior in selected field on return key up
	$('#login_text').keyup(function(e) {
		if (e.keyCode == 13) {
			$('#login_btn').trigger('click');
		}
	});

	// binds login button to click listener
	$('#login_btn').click(
			function(e) {
				var loginName = $('#login_text').val();
				if (!loginName || loginName == '') {
					alert("You have entered no login name");

					return;
				}

				var expiry = new Date();
				expiry.setFullYear(expiry.getFullYear() + 1);
				document.cookie = 'login_name=' + loginName + ';expires='
						+ expiry.toString();

				window.location = '/home';
			});
});