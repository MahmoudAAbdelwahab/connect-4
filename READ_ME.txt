Welcome to Connect-4 application!

Prerequisites of running the app:-
* MySQL and Maven installed on device.

How to run the app:-
* Use cmd/terminal to navigate to application's root directory
* Type command: mvn jetty:run

How to use the app:-
* Navigate to "localhost:8080" from Chrome or FireFox
* Kindly don't use Safari, as it encounters unexpected behaviour at runtime unlike Chrome and FireFox